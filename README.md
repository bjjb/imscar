# Imscar

> imscar, v.t. & i. (pp. ~tha).1. Spread about; separate. 2. Mil: Deploy

**Zero-touch deployment tool.**

[`imscar`](./imscar/) is a tool that tries very hard to deploy something. It's
designed to make deploying from the SCM provided CI systems of GitLab,
BitBucket, Circle-CI, etc., as easy as possible. In the ideal case, you simply
use the image [`bjjb/imscar`](https://hub.docker.com/bjjb/imscar) and run the
single command `imscar` as the dpeloyment step - the tool will try to infer
what to do from the environment, configuration, and so-forth.

It is also a library for managing cloud-native deployments.

![Ship it!](./logo.svg 'Logo © Zlatko Najdenovski')
