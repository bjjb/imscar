# start with the official Go 1.13 image (not alpine, which lacks gcc)
FROM golang:1.13 AS builder
# add the current working directory to $GOPATH/src
ADD . /go/src/imscar
# ...and work from there
WORKDIR /go/src/imscar
# build the project as normal, creating the imscar executable
ENV CGO_ENABLED=0
ENV GOOS=linux
RUN go build -a -installsuffix cgo -ldflags '-extldflags "static"'
# from a tiny image...
FROM scratch
# ...copy the executable from the builder
COPY --from=builder /go/src/imscar/imscar /imscar
WORKDIR /
# ...and that's the command.
CMD ["./imscar"]
