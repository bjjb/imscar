.PHONY: all test clean image publish

VERSION				?= 0.0.1
VCS 					?= bitbucket.org
OWNER 				?= bjjb
PROJECT 			?= imscar
IMAGE_TAG			?= $(OWNER)/$(PROJECT)
FULL_PROJECT 	?= $(VCS)/$(OWNER)/$(PROJECT)
GOVERSION 		?= 1.13

# Runs go test in a Docker image
test:
	@docker run \
		-v $(PWD):/go/src/$(FULL_PROJECT) \
		-w /go/src/$(FULL_PROJECT) \
		golang:$(GOVERSION) \
		go test

clean:
	:

# Builds the Docker image
image:
	docker build -t $(IMAGE_TAG) .

# Publishes the Docker image
publish: image
	docker push $(IMAGE_TAG)
